const express = require('express');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

const mongoDB = 'mongodb://localhost/red_bicicletas';

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

mongoose.connect(mongoDB);
const db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDb connection error'));
db.once('open', function(){
    console.log("-- App connected to Mongo --");
});


const indexRouter = require('./routes/index');
const bicicletasRouter = require('./routes/bicicletas');
const bicicletasAPIRouter = require('./routes/api/bicicletas');
const usuariosAPIRouter = require('./routes/api/usuarios');

// settings
app.set('port', process.env.PORT || 3000);
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

// middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));

app.use('/',indexRouter); 
app.use('/bicicletas/',bicicletasRouter);  
app.use('/api/bicicletas',bicicletasAPIRouter); 
app.use('/api/usuarios',usuariosAPIRouter);
   

app.listen(app.get('port'), () => console.log(`- Nodejs server listening at http://localhost:${app.get('port')} -`));