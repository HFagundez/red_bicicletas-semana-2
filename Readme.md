Curso:
Desarrollo del lado servidor: NodeJS, Express y MongoDB

Entrega - Semana 2

- No olvidar realizar npm install para cargar los módulos.

- Para correr los tests:
    iniciar el servidor de mongo con: mongod
    iniciar la app con: npm run devstart
    iniciar test con: npm test 
    
- Se adjunta zip con capturas de los tests realizados y otros requerimientos


Horacio Fagundez - hfagundez@protonmail.com